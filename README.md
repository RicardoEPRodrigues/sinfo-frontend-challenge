# MB.io - SINFO Frontend Challenge

> Submission of Ricardo Rodrigues (ricardo.e.p.rodrigues@gmail.com)

## Personal Notes

I decided to use Lory, since it is recommended by the MB.io team, yet there are problems related with the way it calculates the next steps of the slides to show. By downloading the `master` branch version and digging around I was able to correct the problem. (compare files to see the small, yet significant, differences).

The UI is responsive. I used CSS media queries and created a resize event to modify lory when the window was resized (mimicking Media Queries behavior in JS).

In the last challenges (Bonus #2 and #3) I decided to use Grunt to minify and package my code. Additionally I also implemented Live Reload. See [the Grunt section](#installing-grunt)

For a reimplementation of the Slider I decided to break the rules and create an incomplete one. Why? Because it uses only CSS for behavior!

The final pages shows two sliders, the top one uses Lory and the bottom one uses CSS only.

![Final Page with 2 Sliders](https://bitbucket.org/RicardoEPRodrigues/sinfo-frontend-challenge/raw/master/design/shop_w_slider_final.png)

## Setup your system

### TL;DR

1. Install node.js
1. Install Grunt
1. Run `npm install`
1. The website is now live!
1. After every modification run `grunt`.

### node.js

To automatically serve the challenge's index.html page and for additional tasks you may need (section [Requirements](#requirements), steps 6. and 7.), we recommend you use [node.js](https://nodejs.org) v8.x but any other recent (>=6.0.0) version should be fine also. In case you want to manage several node versions on your machine, we recommend [nvm](https://github.com/creationix/nvm) for Mac/*nix and [nvm-windows](https://github.com/coreybutler/nvm-windows) for Windows.

### Installing Grunt

[Grunt](https://gruntjs.com/) is a JavaScript Task Runner that allows automation of repetitive tasks. This project uses Grunt to minify and package file to the `dist` folder and to live reload pages in development.

To install Grunt-cli run:

```
npm install -g grunt-cli
```

You may need sudo on *nix systems (for Ubuntu run: `sudo apt install node-grunt-cli`).

### Install NPM packages and run dev

Once you have node.js installed, open up your OS's Terminal/Bash application, change into the root folder of the challenge (where the file `package.json` is located) and run `npm install`.

After all NPM packages installed successfully it will open a browser tab with src/index.html running in http://localhost:8080/ (if you can't see that tab in your browser try running `npm run dev`).


### Grunt usage and Live Reload

After installing the needed NPM packages, we can use grunt. To run it simply type `grunt` on a terminal at the root of the project. Anytime you change your code, remember to run it so the server code is updated.

To help in the development process, live reload allows you to see the changes immediately without reloading the page. To activate live reload run `grunt watch` on a terminal at the root of the project. With watch running, anytime you package your project (`grunt`) the page will update.

## Coding guidelines

1. HTML markup
    Make sure the document contains semantically correct HTML5 markup [[help]](http://html5doctor.com/lets-talk-about-semantics/).

2. Accessibility
    - Fully functional with keyboard.
    - Tab navigation follows a logical flow.
    - Use buttons where buttons are needed.
    - Correct usage of ARIA labels and landmark roles.

3. CSS (you can also use SASS, we like it - OPTIONAL)
    - You can use one or several CSS OR SCSS files, separated by context, modules, etc.
    - You are not allowed to use any CSS or SASS frameworks (e.g. Bootstrap), but you may include a small third-party CSS reset / normalize.
    - Your CSS should be markup independent. E.g. no class names as `.h1` or `.table`.
    - Make sure your selectors have a low specificity [[help]](https://css-tricks.com/strategies-keeping-css-specificity-low/)
    - avoid long CSS selectors. E.g. use `.hilite-heading--small` instead of `body .main section.top .hilites h3.small`

4. JavaScript
    - Use "vanilla" JavaScript.
    - Third-party libraries and frameworks are not allowed (e.g. jQuery, React, Vue.js, etc).
    - As an exception to the above, you should use Lory - http://meandmax.github.io/lory/ - for the slider.
    - Avoid generating completely new HTML structures from within JavaScript.
    - Use comments when necessary.

## Where should I place my files?

We've got an initial folder and file structure in `src` prepared for you:

* `index.html` - HTML file with prepared markup and included assets.
* `vendor/main.js` - this is your main JS file
* `images` - place images in here
* `vendor` - if using Lory (for the slider) place its JavaScript and/or CSS/SCSS files in here
