document.addEventListener('DOMContentLoaded', function () {
  var slider_dom = document.querySelector('.js_slider');

  var options = {
    slidesToScroll: 3,
    enableMouseEvents: true,
  };

  var slider = lory(slider_dom, options);

  function onResize() {
    var windowWidth = window.innerWidth;
    var slidesToScroll = options.slidesToScroll;
    if (windowWidth >= 880) {
      slidesToScroll = 3;
    } else if (windowWidth >= 620) {
      slidesToScroll = 2;
    } else {
      slidesToScroll = 1;
    }

    if (slidesToScroll !== options.slidesToScroll) {
      options.slidesToScroll = slidesToScroll;
      slider.setup();
    }
  }

  onResize();
  slider_dom.addEventListener('on.lory.resize', onResize);

  // Utilitary

  function addListener(element, eventName, handler) {
    if (element.addEventListener) {
      element.addEventListener(eventName, handler, false);
    } else if (element.attachEvent) {
      element.attachEvent('on' + eventName, handler);
    } else {
      element['on' + eventName] = handler;
    }
  }

  function removeListener(element, eventName, handler) {
    if (element.addEventListener) {
      element.removeEventListener(eventName, handler, false);
    } else if (element.detachEvent) {
      element.detachEvent('on' + eventName, handler);
    } else {
      element['on' + eventName] = null;
    }
  }

  function formatNumber(number) {
    return number.toFixed(2).replace(".", ",");
  }

  // Functionality implementation

  function Item() {
    this.id = "item";
    this.amount = 0;
    this.price = 0;
    this.setId = function (id_val) {
      this.id = id_val;
    };
    this.setPrice = function (price_val) {
      this.price = price_val;
    };
    this.setAmount = function (amount_val) {
      if (amount_val < 0) {
        return;
      }
      this.amount = amount_val;
    };
    this.minusAmount = function () {
      if (this.amount == 0) {
        return;
      }
      this.amount--;
    };
    this.plusAmount = function () {
      this.amount++;
    }
  }

  function ShoppingCart(element) {
    var items = [];

    var total_dom = element.querySelector(".total");

    function updateTotal() {
      var value = 0;
      items.forEach(function (item) {
        value += item.price * item.amount;
      });
      total_dom.textContent = formatNumber(value);
    }

    var items_dom = element.querySelectorAll(".item");
    items_dom.forEach(function (item_dom) {
      // create new item
      var item = new Item();
      // place it in item list
      items.push(item);
      // set ID
      item.setId(item_dom.id);
      // set Price
      var price = item_dom.querySelector(".prize");
      item.setPrice(parseFloat(price.textContent.replace(",", ".")));
      // set Callback for buttons
      var minus = item_dom.querySelector(".minus-button");
      var plus = item_dom.querySelector(".plus-button");
      var amount = item_dom.querySelector(".amount");
      amount.value = 0; // If previous input was placed remove it.
      addListener(minus, 'click', function () {
        item.minusAmount();
        amount.value = item.amount;
        updateTotal();
      });
      addListener(plus, 'click', function () {
        item.plusAmount();
        amount.value = item.amount;
        updateTotal();
      });
      addListener(amount, 'input', function () {
        item.setAmount(this.value);
        amount.value = item.amount;
        updateTotal();
      });
    });
  }

  ShoppingCart(document.getElementById("lory"));
  ShoppingCart(document.getElementById("no-js"));
});
